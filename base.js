const mysql = require('mysql');
const crypto = require('crypto');

class Database {
    /**
     * Base controller constructor.
     *
     * @param {string} host
     * @param {string} database
     * @param {string} username
     * @param {string} password
     */
    constructor( host, database, user, password ) {
        this.connection = mysql.createPool({
            connectionLimit : 50,
            host: host,
            database: database,
            user: user,
            password: password
        });
    }
    /**
     * Get function
     *
     * @param {string} table
     * @param {string|object} where parameters - single string is used like id by default
     */
    get(table, where = null) {
        let sql = `SELECT * FROM ${table}`;
        if(where !== null) {
            if(typeof where !== 'object') {
                sql += ` WHERE id='${where}'`;
            }
            else {
                let first = true;
        
                for (let key in where) {
                    if(first) sql += ` WHERE `;
                    else sql += ` AND`;
                    sql += `${key}='${where[key]}'`;
                    first = false;
                }
            }
        }

        return new Promise( ( resolve, reject ) => {
            this.connection.getConnection((err, connection) => {
                // Handle error after the release.
                if (err) return reject( err );

                // Use the connection
                connection.query(sql, (err, rows) => {
                    let result = {};   
                    rows.forEach(function(row) {
                        result[row.id] = {...row}
                        delete result[row.id].id;
                    });
                    resolve( result );
                    connection.release();
                });
            });
        } );
    }
    /**
     * Insert function
     *
     * @param {string} table
     * @param {object} data - string id is generated automatically
     */
    insert(table, data) {
        let id = crypto.randomBytes(16).toString('hex');
        let keys = `id`;
        let values = `'${id}'`;
        for (let key in data) {
            keys += `, ${key}`;
            values += `, '${data[key]}'`;
        }

        return new Promise( ( resolve, reject ) => {
            this.connection.getConnection((err, connection) => {
                // Use the connection
                    connection.query(`INSERT INTO ${table} (${keys}) VALUES (${values})`, ( err, response ) => {
                        resolve( {id: id, response: response} );
                        connection.release();
                        // Handle error after the release.
                        if (err) return reject( err );
                    });
            } );
        });
    }
    /**
     * Update function
     *
     * @param {string} table
     * @param {string} row's id
     * @param {object} data
     */
    update(table, id, data) {
        let values = ``, first = true;

        for (let key in data) {
            if(!first) {
                values += `, `;
            }
            else first = false;

            values += `${key}='${data[key]}'`;
        }

        return new Promise( ( resolve, reject ) => {
            this.connection.getConnection((err, connection) => {
                // Use the connection
                    connection.query(`UPDATE ${table} SET ${values} WHERE id='${id}'`, ( err, response ) => {
                        resolve( response );

                        connection.release();
                        // Handle error after the release.
                        if (err) return reject( err );
                    });
            });
        });
    }
    /**
     * Delete function
     *
     * @param {string} table
     * @param {string} row's id
     */
    delete(table, id) {
        return new Promise( ( resolve, reject ) => {
            this.connection.getConnection((err, connection) => {
                connection.query(`DELETE FROM ${table} WHERE id='${id}'`, ( err, response ) => {
                    resolve( response );
                    connection.release();
                    // Handle error after the release.
                    if (err) return reject( err );
                });
            });
        });
    }
}

module.exports = Database;