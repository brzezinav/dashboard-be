require('dotenv').load();
const fs = require('fs');
const port = process.env.PORT || 8080;
const http = require('http');
const https = require('https');
const app = require('express')();
const session = require('express-session');
const Database = require('./base');

const nodemailer = require('nodemailer');
const CronJob = require('cron').CronJob;

const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const OAuth = require('oauth').OAuth;
const google = require('googleapis');
const OAuth2 = google.auth.OAuth2;

const googleOauth2Client = new OAuth2(
  '767977546830-ae438ji7e09305dqv5hoh6o3hr8kotg3.apps.googleusercontent.com',
  '9G3fNPUTwvCDAtVvnyDcU4jy',
  process.env.HOST + ":" + port + "/auth"
);

// set auth as a global default
google.options({
    auth: googleOauth2Client
});

const calendar = google.calendar('v3');

const consumer = new OAuth(process.env.JIRA_API + "/plugins/servlet/oauth/request-token", //request token
    process.env.JIRA_API + "/plugins/servlet/oauth/access-token", //access token
    "dashboard", //consumer key 
    fs.readFileSync(process.env.JIRA_KEY, 'utf8'),
    '1.0', //OAuth version
    process.env.HOST + ":" + port + "/jira/auth", //callback url
    "RSA-SHA1"
);

moment.locale('cs', {
    weekdaysShort : ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
    week : {
        dow : 1,
        doy : 4
    },
    months: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec']
});

if(process.env.SECURE && process.env.KEY && process.env.CERT && process.env.CA) {
    var server = https.createServer({
        key: fs.readFileSync(process.env.KEY),
        cert: fs.readFileSync(process.env.CERT),
        ca: fs.readFileSync(process.env.CA)
    }, app).listen(port)
}
else {
    var server = http.createServer(app).listen(port)
}

var io = require('socket.io')(server);

var database = new Database(process.env.DB_HOST, process.env.DB_TABLE, process.env.DB_USER, process.env.DB_PASSWORD);

if (process.env.MAIL_HOST && process.env.MAIL_PORT){
    var transporter = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        }
    });

    /**
     * CRON and content settings
     */
        
    new CronJob('00 00 08 * * 1', () => {
        database.get('users')
            .then((users) => {
                Object.keys(users).map(key => {
                    database.get('attendance', {uid: key}).then((attendance) => {
                        let count = 0;
                        Object.keys(attendance).map(key => {
                            let range = moment.range(moment().startOf('week'), moment().endOf('week'));
                            if(moment(attendance[key].start).within(range)) count++;
                        })
                        if(count === 0) {
                            // setup email data with unicode symbols
                            let mailOptions = {
                                from: '"Evolve Dashboard" <dashboard@brzezina.online>', // sender address
                                to: users[key].email, // list of receivers
                                subject: "Tvá přítomnost", // Subject line
                                text: 'Ahoj, nezapomeň svým kolegům sdělit svou přítomnost na tento týden!', // plain text body
                                html: '<p>Ahoj, nezapomeň svým kolegům sdělit svou přítomnost na tento týden!</p>' // html body
                            };
    
                            // send mail with defined transport object
                            transporter.sendMail(mailOptions, (error, info) => {
                                if (error) {
                                    return console.log(error);
                                }
                            });
                        }
                    }).catch(function (err) {
                        console.info(err);
                    });
                })
            }).catch(function (err) {
                console.info(err);
        });
      }, null, true, 'Europe/Prague');

}

app.use(function(req, res, next) {
    res.set({
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        "Access-Control-Allow-Origin": process.env.HOST,
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
        'Content-Type': 'text/json'
      })
    next();
});

/**
 * Session init
 */
app.use(session({ secret: 'jira token', cookie: {path: '/', httpOnly: false, secure: process.env.SECURE == true ? true : false, maxAge: 60 * 60 * 60 * 24 * 7}, resave: true,saveUninitialized: true}));

module.exports = {
    server: server,
    database: database,
    app: app,
    io: io,
    consumer: consumer,
    calendar: calendar,
    googleOauth2Client: googleOauth2Client
}