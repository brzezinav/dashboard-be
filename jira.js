const fetch = require('node-fetch');
const consumer = require('./config').consumer;

class Jira {
    constructor(oauthAccessToken, oauthAccessTokenSecret) {
        this.uri = process.env.JIRA_API + '/rest/api/latest';
        this.oauthAccessToken = oauthAccessToken;
        this.oauthAccessTokenSecret = oauthAccessTokenSecret;
    }
    get_user() {
        return this.sendRequest(`${this.uri}/myself`, 'GET');
    }
    get_issues(username) {
        return this.sendRequest(`${this.uri}/search?jql=assignee=${username} AND status in ("in progress", "open", "to do", "waiting") ORDER BY priority`, 'GET');
    }
    get_issue(key) {
        return this.sendRequest(`${this.uri}/issue/${key}`, 'GET');
    }
    get_transitions(issue) {
        return this.sendRequest(`${this.uri}/transitions/${issue}`, 'GET');
    }
    sendRequest(uri, method, data) {
        return new Promise((resolve, reject) => {
            consumer.get(uri,
                this.oauthAccessToken,
                this.oauthAccessTokenSecret,
                function (error, data, resp) {
                    if(error) reject(error);
                    else resolve(JSON.parse(data));
                }
            );
        });
    }
}

module.exports = Jira;