### 1.A Create a new folder from repository

    git clone git clone https://brzezinav@bitbucket.org/brzezinav/dashboard-be.git
    cd dashboard-be

### 1.B Init repository in existing folder

    cd existing_folder
    git init
    git remote add origin git clone https://brzezinav@bitbucket.org/brzezinav/dashboard-be.git
    git pull origin master

### 2 Install dependencies

    npm install

### 3.A Run project in debug with nodemon

    npm debug

### 3.B Run project with forever

    npm start

### 4.B Stop project with forever

    npm stop