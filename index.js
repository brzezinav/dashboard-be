const config = require('./config');
const app = config.app;
const database = config.database;
const io = config.io;
const consumer = config.consumer;
const googleOauth2Client = config.googleOauth2Client;
const calendar = config.calendar;
const sha256 = require("crypto-js/sha256");
const Jira = require('./jira');

/**
 * Auth middleware
 */
app.use(function (req, res, next) {
    if(req.path == '/auth' || req.path == '/jira/auth' || req.path.startsWith('/jira/hook/')) next();
    else {
        if (req.session.oauthAccessToken != undefined || req.session.oauthAccessTokenSecret != undefined) {
            req.jira = new Jira(req.session.oauthAccessToken, req.session.oauthAccessTokenSecret );
            next();
        }
        else {
            res.json({error: 'authentication required'});
        }
    }
});

app.get('/auth', (req, res) => {
    var googleAuthUrl = googleOauth2Client.generateAuthUrl({
        // 'online' (default) or 'offline' (gets refresh_token)
        access_type: 'offline',
    
        // If you only need one scope you can pass it as a string
        scope: 'https://www.googleapis.com/auth/calendar',
    
        // Optional property that passes state parameters to redirect URI
        // state: 'foo'
    });

    return res.redirect(googleAuthUrl);
});

app.get('/jira/auth', (req, res) => {

    googleOauth2Client.getToken(req.query.code, function (err, tokens) {
        // Now tokens contains an access_token and an optional refresh_token. Save them.
        if (!err) {
            googleOauth2Client.credentials = tokens;
        }
      });

    //Session is not set
    if (req.session.oauthRequestToken == undefined || req.session.oauthRequestTokenSecret == undefined) {
        consumer.getOAuthRequestToken(function (error, oauthToken, oauthTokenSecret) {
            if (error) {
                res.send('Error getting OAuth access token');
            } else {
                req.session.oauthRequestToken = oauthToken;
                req.session.oauthRequestTokenSecret = oauthTokenSecret;
                req.session.save();
                return res.redirect(process.env.JIRA_API + "/plugins/servlet/oauth/authorize?oauth_token=" + oauthToken);
            }
        });
    }
    else {
        if(req.session.oauthAccessToken == undefined || req.session.oauthAccessTokenSecret == undefined) {
            consumer.getOAuthAccessToken(
                req.session.oauthRequestToken,
                req.session.oauthRequestTokenSecret,
                req.query.oauth_verifier,
                function (error, oauthAccessToken, oauthAccessTokenSecret, results) {
                    if (error) {
                        res.json({error:'getOAuthAccessTokenError', content: error});
                    }
                    else {
                        req.session.oauthAccessToken = oauthAccessToken;
                        req.session.oauthAccessTokenSecret = oauthAccessTokenSecret;
                        req.session.save();

                        jira = new Jira(oauthAccessToken, oauthAccessTokenSecret )

                        jira.get_user()
                        .then((user) => {
                            database.get('users', { email: user.emailAddress })
                                .then((data) => {
                                    //User is not in our database so we'll create a record
                                    if(data.constructor === Object && Object.keys(data).length === 0) {
                                        let insert = {
                                            email: user.emailAddress,
                                            name: user.name,
                                            displayName: user.displayName,
                                            token: sha256(req.query.token)
                                        };
                                        database.insert('users', insert)
                                            .then((response) => {
                                                res.redirect(process.env.HOST)
                                            }).catch(function (err) {
                                                res.json(err);
                                        });
                                    }
                                    else {
                                        res.redirect(process.env.HOST)
                                    }
                                }).catch(function (err) {
                                    res.json(err);
                                });
                        })
                        .catch((err) => {
                            res.json({ error: err });
                        });

                    }
                }
            )
        }
        else {
            res.json({
                oauthAccessToken: req.session.oauthAccessToken,
                oauthAccessTokenSecret: req.session.oauthAccessTokenSecret,
                googleOauth2ClientCredentials: googleOauth2Client.credentials
            })
        }
    }
});

app.get('/google/calendar/events', (req, res) => {
    calendar.events.list({
        calendarId: 'primary'
    }, function (err, response) {
        if(err !== null) {
            console.info(err)
        }
        else {
            res.json(response)
        }
    });
});

app.get('/base/get/:table/:id*?', (req, res) => {
    database.get(req.params.table, req.params.id !== '' ? req.params.id : null)
        .then((data) => {
            res.json(data);
        }).catch(function (err) {
            res.json(err);
        });
});

app.get('/base/myself', (req, res) => {
    req.jira.get_user()
    .then((user) => {
        database.get('users', { email: user.emailAddress })
            .then((response) => {
                res.json(response);
            }).catch(function (err) {
                res.json(err);
            });
    })
    .catch((err) => {
        res.json({ error: err });
    });
});

app.get('/base/set/:table', (req, res) => {
    database.insert(req.params.table, JSON.parse(req.query.data))
        .then((response) => {
            res.json(response);
        }).catch(function (err) {
            res.json(err);
        });
});

app.get('/base/update/:table/:id', (req, res) => {
    database.update(req.params.table, req.params.id, JSON.parse(req.query.data))
        .then((response) => {
            res.json(response);
        }).catch(function (err) {
            res.json(err);
        });
});

app.get('/base/remove/:table/:id', (req, res) => {
    database.delete(req.params.table, req.params.id)
        .then((response) => {
            res.json(response);
        }).catch(function (err) {
            res.json(err);
        });
});

app.get('/jira/logout', (req, res, jira) => {
    return new Promise((resolve, reject) => {
        req.session.destroy((err) => {
            if (err) resolve(err);
            else resolve({ error: false });
        });
    }).then((result) => {
        res.json(result);
    });
});

app.get('/jira/issue/:issue', (req, res) => {
    req.jira.get_issue(req.params.issue)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.json({ error: err });
        });
});

app.get('/jira/issues/:username', (req, res) => {
    req.jira.get_issues(req.params.username)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.json({ error: err });
        });
});

app.get('/jira/transitions/:issue', (req, res) => {
    req.jira.get_transitions(req.params.issue)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.json({ error: err });
        });
});

app.get('/jira/hook/update-issue/:issue', (req, res) => {
    console.info(req.params.issue, req);
    io.emit('update:issue', 'req')
    res.write('updated');
});

io.on('connection', function (socket) {
    socket.on('update', (action) => {
        socket.broadcast.emit('update:' + action.table, action.payload);
    });
    socket.on('insert', (action) => {
        socket.broadcast.emit('insert:' + action.table, action.payload);
    });
    socket.on('delete', (action) => {
        socket.broadcast.emit('delete:' + action.table, action.id);
    });
});